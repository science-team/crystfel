.\"
.\" Geometry man page
.\"
.\" Copyright © 2012-2014 Thomas White <taw@physics.org>
.\"
.\" Part of CrystFEL - crystallography with a FEL
.\"

.TH CRYSTFEL\_GEOMETRY 5
.SH NAME
CRYSTFEL DETECTOR GEOMETRY AND BEAM DESCRIPTION FILES

See below for information about CrystFEL's beam description files.

.SH CRYSTFEL DETECTOR GEOMETRY FILES
The detector geometry is taken from a text file rather than hardcoded into the
program.  Programs which care about the geometry (particularly
\fBindexamajig\fR and \fBpattern_sim\fR take an argument
\fB--geometry=\fR\fIfilename\fR, where \fIfilename\fR contains the geometry.
.PP
A flexible (and pedantic) representation of the detector has been developed to
avoid all possible sources of ambiguity.  CrystFEL's representation of a
detector is broken down into one or more "panels", each of which has its own
camera length, geometry, resolution and so on.  Each panel fits into the overall
image taken from the HDF5 file, defined by minimum and maximum coordinates in
the "fast scan" and "slow scan" directions.  "Fast scan" refers to the direction
whose coordinate changes most quickly as the bytes in the HDF5 file are moved
through.  The coordinates are specified inclusively, meaning that a minimum of 0
and a maximum of 9 results in a width of ten pixels.  Counting begins from zero.
All pixels in the image must be assigned to a panel - gaps are not permitted.
.PP
In the current version, panels are assumed to be perpendicular to the incident
beam and to have their edges parallel.  Within these limitations, any geometry
can be constructed.

The job of the geometry file is to establish a relationship between the array
of pixel values in the HDF5 file, defined in terms only of the "fast scan" and
"slow scan" directions, and the laboratory coordinate system defined as follows:

.IP
+z is the beam direction, and points along the beam (i.e. away from the source)

.IP
+y points towards the zenith (ceiling).

.IP
+x completes the right-handed coordinate system.

.PP
Naively speaking, this means that CrystFEL looks at the images from the "into the
beam" perspective, but please avoid thinking of things in this way.  It's much
better to consider the precise way in which the coordinates are mapped.

The geometry file should contain lines of the following form:

.IP
\fIpanel\fR\fB/\fIproperty\fB = \fIvalue\fR

.PP
\fIpanel\fR can be any name of your choosing.  You can make up names for your panels however you please, as long as the first three letters are not "\fBbad\fR" (in lower case), because this is used to identify bad regions.

.PP
You can also specify values without a panel name, for example:

.IP
peak_sep = 6.0

.PP
In this case, the value will be used for all \fBsubsequent\fR panels appearing in the file which do not have their own specific values for the property, or until you specify another default value further down the file.  Panel-specific values always have priority over default values, and changing the default value has no effect for panels which had already be mentioned at the point in the file where the default value was specified.

.PP
Lines which should be ignored start with a semicolon.

.PP
The properties which can be set are:

.PD 0
.IP \fBmin_fs\fR
.IP \fBmin_ss\fR
.IP \fBmax_fs\fR
.IP \fBmax_ss\fR
.PD
The range of pixels in the HDF5 file which correspond to the panel, in fast scan/slow scan coordinates, specified \fBinclusively\fR.

.PD 0
.IP \fBadu_per_eV\fR
.PD
The number of detector intensity units (ADU) which will arise from one electron-Volt of photon energy.  This is used to estimate Poisson errors.

.PD 0
.IP \fBbadrow_direction\fR
.PD
This is the readout direction of a CCD panel, and can be \fBf\fR, \fBs\fR or \fB-\fR.
If more than three peaks are found in the same readout region with similar coordinates perpendicular to the readout direction, they will all be discarded.  This helps to avoid problems due to streaks appearing along the readout direction, which has happened in the past with pnCCD detectors.
If the badrow direction is '-', then the culling will not be performed for this panel.

.PD 0
.IP \fBres\fR
The resolution (in pixels per metre) for this panel.  This is one over the pixel size in metres.

.PD 0
.IP \fBclen\fR
.PD
The camera length (in metres) for this panel. You can also specify the HDF path to a scalar floating point value containing the camera length in millimetres.  For example: "panel0/clen = /LCLS/detectorPosition".  See \fBcoffset\fR as well.

.PD 0
.IP \fBcoffset\fR
.PD
The camera length offset (in metres) for this panel.  This number will be added to the camera length (\fBclen\fR).  This can be useful if the camera length is taken from the HDF5 file and you need to make an adjustment, such as that from a calibration experiment.

.PD 0
.IP \fBfs\fR
.IP \fBss\fR
.PD
For this panel, the fast and slow scan directions correspond to the given directions in the lab coordinate system described above, measured in pixels.  Example: "panel0/fs = 0.5x+0.5y".

.PD 0
.IP \fBcorner_x\fR
.IP \fBcorner_y\fR
.PD
The corner of this panel, defined as the first point in the panel to appear in the HDF5 file, is now given a position in the lab coordinate system. The units are pixel widths of the current panel.  Note that "first point in the panel" is a conceptual simplification.  We refer to that corner, and to the very corner of the pixel - not, for example, to the centre of the first pixel to appear.

.PD 0
.IP \fBmax_adu\fR
The maximum value, in ADU, before the pixel will be considered as bad.  That is, the saturation value for the panel.

.PD 0
.IP \fBno_index\fR
Set this to 1 or "true" to ignore this panel completely.

.PD 0
.IP \fBmask\fR
.IP \fBmask_good\fR
.IP \fBmask_bad\fR
.PD
If you have a bad pixel mask, you can include it in the HDF5 file as an unsigned 16-bit integer array of the same size as the data.  You need to give its path within each HDF5 file, and two bitmasks.  The pixel is considered good if all of the bits which are set in \fBmask_good\fR are set, \fIand\fR if none of the bits which are set in \fBmask_bad\fR are set. Example:
.br
mask = /processing/hitfinder/masks
.br
mask_good = 0x27
.br
mask_bad = 0x00


.SH BAD REGIONS

You can also specify bad regions.  Peaks with centroid locations within such a region will not be integrated nor used for indexing.  Bad regions are specified in pixel units, but in the lab coordinate system (see above).  Bad regions are distinguished from normal panels by the fact that they begin with the three letters "bad".

Example:
.br
badregionA/min_x = -20.0
.br
badregionA/max_x = +20.0
.br
badregionA/min_y = -100.0
.br
badregionA/max_y = +100.0

.PP
See the "examples" folder for some examples (look at the ones ending in .geom).

.SH CRYSTFEL BEAM DESCRIPTION FILES
CrystFEL beam description files, usually given with \fB--beam=\fR\fIfilename\fR,
describe the beam parameters.  The syntax of each line in the beam file is simply this:

.IP
\fIparameter\fB = \fIvalue\fR

.PP
The possible parameters are:

.PD 0
.IP \fBbeam/fluence\fR
.PD
The number of photons per pulse.

.PD 0
.IP \fBbeam/radius\fR
.PD
The radius of X-ray beam, in metres.

.PD 0
.IP \fBbeam/photon_energy\fR
.PD
The photon energy in electron-Volts, or an HDF5 path to a stored wavelength value, also in eV.

.PD 0
.IP \fBbeam/bandwidth\fR
.PD
Bandwidth: FWHM(wavelength) over wavelength.  Note: current simulation code just uses a rectangular distribution with this as its (full) width.

.PD 0
.IP \fBbeam/divergence\fR
Beam divergence (full convergence angle, \fBnot\fR the half-angle) in radians.

.PD 0
.IP \fBprofile_radius\fR
.PD
Reciprocal space 3D profile radius in m^-1.  A sphere of this radius surrounds each reciprocal space, and if any part of the sphere is inside the excited volume of reciprocal space, the reflection will be predicted.  You can change the prediction of spots by altering this value - larger numbers give more spots;

.PP
The parameters \fBbeam/fluence\fR and \fBbeam/radius\fR are only relevant when simulations, e.g. with pattern_sim.  \fBbeam/bandwidth\fR, \fBbeam/divergence\fR and \fBprofile_radius\fR affect which spots are predicted for the final stages of integration.

.SH AUTHOR
This page was written by Thomas White.

.SH REPORTING BUGS
Report bugs to <taw@physics.org>, or visit <http://www.desy.de/~twhite/crystfel>.

.SH COPYRIGHT AND DISCLAIMER
Copyright © 2012-2014 Deutsches Elektronen-Synchrotron DESY, a research centre of the Helmholtz Association.
.P
CrystFEL is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
.P
CrystFEL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
.P
You should have received a copy of the GNU General Public License along with CrystFEL.  If not, see <http://www.gnu.org/licenses/>.

.SH SEE ALSO
.BR crystfel (7),
.BR pattern_sim (1),
.BR indexamajig (1)
