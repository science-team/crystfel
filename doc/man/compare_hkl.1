.\"
.\" compare_hkl man page
.\"
.\" Copyright © 2012 Thomas White <taw@physics.org>
.\"
.\" Part of CrystFEL - crystallography with a FEL
.\"

.TH COMPARE_HKL 1
.SH NAME
compare_hkl \- compare reflection data
.SH SYNOPSIS
.PP
\fBcompare_hkl\fR \fR [\fIoptions\fR] \fB...\fR \fIfile1.hkl\fR \fIfile2.hkl\fR
.PP
\fBcompare_hkl --help\fR

.SH DESCRIPTION
compare_hkl compares two sets of reflection data and calculates figures of merit such as R-factors.  Reflections will be considered equivalent according to your choice of point group.

.SH OPTIONS
.PD 0
.IP "\fB-y\fR \fpointgroup\fR"
.IP \fB--symmetry=\fR\fIpointgroup\fR
.PD
Specify the symmetry of the reflections.  The symmetry must be the same for both lists of reflections.  Default: 1 (no symmetry).

.PD 0
.IP "\fB-p\fR \fIunitcell.pdb\fR"
.IP \fB--pdb=\fR\fIunitcell.pdb\fR
.PD
Specify the name of the PDB file containing at least a CRYST1 line describing the unit cell.

.PD 0
.IP \fB--fom=\fR\fIFoM\fR
.PD
Calculate figure of merit \fIFoM\fR.  Possible figures of merit are:
.RS
.IP \fBRsplit\fR
.PD
2^(-0.5) * sum(|I1-kI2|) / [ 0.5*sum(I1+kI2) ]
.IP \fBR1f\fR
.PD
sum(sqrt(I1)-sqrt(kI2)) / sum(sqrt(I1))
.IP \fBR1i\fR
.PD
sum(|I1-kI2|) / sum(I1)
.IP \fBR2\fR
.PD
sqrt(sum[(I1-kI2)^2] / sum(I1^2))
.IP \fBCC\fR
.PD
The Pearson correlation coefficient.
.IP \fBCC*\fR
.PD
See Karplus and Diederichs, Science 336 (2012) p1030.
.IP \fBCCano\fR
.PD
The correlation coefficient of the Bijvoet differences of acentric reflections.
.IP \fBCRDano\fR
.PD
RMS anomalous correlation ratio: The anomalous differences from each data set are plotted in a scatter graph, and the variance along both diagonals measured.  See Evans, Acta Crystallographica D62 (2006) p72.
.IP \fBRano\fR
.PD
sum(|I+ - I-|) / 0.5*sum(I+ - I-)
Note that I+ will be taken to be the mean of the I+ values from both data sets, and likewise for I-.
.IP \fBRano/Rsplit\fR
.PD
The ratio of Rano to Rsplit, as defined above.
.PP
I1 and I2 are the intensities of the same reflection in both reflection lists.  The scale factor, k, is given by sum(I1*i2) / sum(I2^2), unless you use \fB-u\fR.
.RE

.PD 0
.IP \fB--nshells=\fR\fIn\fR
.PD
Use \fIn\fR resolution shells.  Default: 10.

.PD 0
.IP \fB-u\fR
.PD
Fix scale factors to unity.

.PD 0
.IP \fB--shell-file=\fIfilename\fR
.PD
Write the figure of merit in resolution shells to \fIfilename\fR.  Default: "shells.dat".

.PD 0
.IP \fB--ignore-negs\fR
.PD
Ignore reflections with negative intensities.

.PD 0
.IP \fB--zero-negs\fR
.PD
Set to zero the intensities of reflections with negative intensities.

.PD 0
.IP \fB--sigma-cutoff=\fR\fIn\fR
.PD
Discard reflections with I/sigma(I) < \fIn\fR.  Default: -infinity (no cutoff).

.PD 0
.IP \fB--rmin=\fR\fI1/d\fR
.PD
Low resolution cutoff, as 1/d in m<sup>-1</sup>.  Use this or \fB--lowres\fR, but not both.

.PD 0
.IP \fB--lowres=\fR\fId\fR
Low resolution cutoff in Angstroms.  Use this or \fB--rmin\fR, but not both.

.PD 0
.IP \fB--rmax=\fR\fI1/d\fR
.PD
High resolution cutoff, as 1/d in m<sup>-1</sup>.  Use this or \fB--highres\fR, but not both.

.PD 0
.IP \fB--highres=\fR\fId\fR
High resolution cutoff in Angstroms.  Use this or \fB--rmax\fR, but not both.

.PD 0
.IP \fB--intensity-shells\fR
.PD
Use intensity shells instead of resolution shells.  The range of shells will start at the intensity of the least intense reflection, and extend from there by 1/5000th of the intensity of the difference between the strongest and weakest reflection in the first reflection list.  The pairs of reflections will also be assigned their bins according to the intensity of the reflection in the first list.
.sp
Because of the hardcoded factor of 1/5000, needed to avoid a very uneven distribution of the number of reflection pairs in each bin, you are advised not to draw strong conclusions from the results of using this option.

.SH AUTHOR
This page was written by Thomas White.

.SH REPORTING BUGS
Report bugs to <taw@physics.org>, or visit <http://www.desy.de/~twhite/crystfel>.

.SH COPYRIGHT AND DISCLAIMER
Copyright © 2012 Deutsches Elektronen-Synchrotron DESY, a research centre of the Helmholtz Association.
.P
compare_hkl, and this manual, are part of CrystFEL.
.P
CrystFEL is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
.P
CrystFEL is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
.P
You should have received a copy of the GNU General Public License along with CrystFEL.  If not, see <http://www.gnu.org/licenses/>.

.SH SEE ALSO
.BR crystfel (7),
.BR check_hkl (1),
.BR render_hkl (1)
